# Final-TeVeO

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Pablo Pintado González 
* Titulación: Grado de Ingeniería en Sistemas de Telecomunicaciones
* Cuenta en laboratorios: pinti
* Cuenta URJC: p.pintado.2018@alumnos.urjc.es
* Video básico (url):https://youtu.be/3iLRnHSjHfU
* Video parte opcional (url):https://youtu.be/lZ4rjAjscEA
* Despliegue (url): https://pablopinti10.pythonanywhere.com/teveo/
* Contraseñas: No son necesarias
* Cuenta Admin Site: pinti/Belmonte14

## Resumen parte obligatoria
* Página Principal: En esta página se mostrará el listado de todas los comentarios que se encuentran en todas las cámaras disponibles en la página ordenadas desde la más reciente a la más antigua. Cada comentario tendrá la información: el autor del comentario, el texto del comentario, la fecha de cuando se escribió, una imagen de lo que había en la cámara en ese instante y un enlace a la información de la cámara.
* Página Cámaras: En esta página, se mostrará al principio del dos botones con dos listas: Listado1.xml y Listado2.xml, que es donde se encuentra la información de las cámaras. Si pulsamos en dichas cámaras, se mostrará la información relativa a cada una: el identificador de la cámara, dos enlaces; una a los detalles de la cámara y otra a la cámara dinámica; el nombre de la cámara y el número de comentarios de dicha cámara. Al principio de la página también habrá una imagen aleatoria entre las cámaras disponibles.
* Página Dinámica: Esta página ofrece las coordenadas de donde se encuentra la cámara, una imagen dinámica que se actualizan cada 30 segundos, un link para dejar un comentario a esa cámara y el listado de los comentarios anteriores en esa cámara.
* Página Estática : Parecida a la cámara dinámica. Se ofrece el nombre y la ubicación de la cámara, un botón para dar "me gusta" a la cámara, y los enlaces de esta cámara: un link para ir a la cámara dinámica, un link para escribir un comentario de esa cámara y un link que ofrece información en formato Json.
* Página JSON: Ofrece información JSON de la cámara pedida.
* Página de Comentarios: Ofrece una imagen de la cámara en ese momento específico que se va a hacer el comentario, un bloque para escribir el comentario, y también se pueden ver los comentarios anteriores en esa cámara.
* Página de Configuración: En esta página se puede configurar el nombre del usuario, el tipo de letra y el tamaño, y generar un link específico para cada usuario con toda la información guardada. 
* Página de ayuda: En esta página se ofrece información de cómo funciona la página.
* Página del admin: Es el terminal del administrador para configurar las cámaras y los comentarios desde un modo administrador.

## Lista partes opcionales

* Favicon: Se ha añadido un favicon.com a la página web.
* Cerrar sesión: Se controla el cierre de sesión, de tal forma que parece que es la primera vez que se visita la página.
* Botón de me gustas: Se lleva la cuenta de la cantidad de me gustas de la página.

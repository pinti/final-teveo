# Importa los módulos necesarios para las pruebas
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from .models import Camera, Comment
import json

from django.test import TestCase, Client
from django.urls import reverse
from unittest.mock import patch
from .models import Camera, Comment
from .views import download_image, read_xml
import os
import xml.etree.ElementTree as ET
from io import StringIO

class TestViews(TestCase):

    def setUp(self):
        # Configuración inicial para las pruebas
        self.camera = Camera.objects.create(id='cam001', name='Cámara de prueba', latitude='50.1234', longitude='10.5678', image_url='http://example.com/image.jpg')
        self.comment = Comment.objects.create(
            id_camera=self.camera,
            text='Comentario de prueba',
            author='TestUser',
            date=timezone.now()  # Establecer la fecha actual
        )

    def test_index_view(self):
        # Prueba para la vista index
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main.html')

    def test_cameras_view_get(self):
        # Prueba para la vista cameras con método GET
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html')

    def test_cameras_view_post(self):
        # Prueba para la vista cameras con método POST
        post_data = {'file_name': 'listado1.xml'}
        response = self.client.post(reverse('camaras'), post_data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html')

    def test_comentario_view_get(self):
        # Prueba para la vista comentario con método GET
        camera_id = self.camera.id
        response = self.client.get(reverse('comentario') + f'?camera_id={camera_id}')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'comentario.html')

    def test_camera_details_view(self):
        # Prueba para la vista camera_details
        camera_id = self.camera.id
        response = self.client.get(reverse('camera_details', kwargs={'camera_id': camera_id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camera_especifica.html')
        self.assertContains(response, 'Comentario de prueba')

    def test_camera_info_json_view(self):
        # Prueba para la vista camera_info_json
        camera_id = self.camera.id
        response = self.client.get(reverse('camera_info_json', kwargs={'camera_id': camera_id}))
        self.assertEqual(response.status_code, 200)
        camera_data = json.loads(response.content)
        self.assertEqual(camera_data['id'], 'cam001')
        self.assertEqual(camera_data['comments_count'], 1)  # Verifica el número de comentarios

    def test_download_image(self):
        # URL de prueba
        img_url = 'http://example.com/image.jpg'

        # Mock para la solicitud HTTP
        with patch('teveo_app.views.requests.get') as mock_requests_get:
            # Simular una respuesta exitosa
            mock_response = mock_requests_get.return_value
            mock_response.status_code = 200
            mock_response.content = b'This is an image content'

            # Llamar a la función download_image
            image_bytes = download_image(img_url)

        # Verificar que la función devuelve datos base64 válidos
        self.assertTrue(image_bytes.startswith('data:image/jpeg;base64,'))

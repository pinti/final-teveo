from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse
from .models import Camera, Comment
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.http import FileResponse
from xml.etree import ElementTree as ET
from django.utils import timezone
import base64
import urllib.request
import requests
from django.http import JsonResponse
import random
from django.contrib.auth import logout

DB_SOURCE = [
    "listado1.xml",
    "listado2.xml"
]
def index(request):
    comments = Comment.objects.all().order_by('-date')
    camera_count = Camera.objects.count()
    comment_count = Comment.objects.count()
    context = {
        'comments': comments,
        'camera_count': camera_count,
        'comment_count': comment_count
    }
    return render(request, "main.html", context)

def read_xml(file_name):
    cameras = []
    tree = ET.parse(file_name)
    root = tree.getroot()

    for cam in root.findall('cam'):
        cam_id = cam.get('id')
        url = cam.find('url').text
        info = cam.find('info').text
        latitude = cam.find('place/latitude').text
        longitude = cam.find('place/longitude').text

        camera, created= Camera.objects.update_or_create(id=cam_id, defaults={'name': info, 'latitude': latitude, 'longitude': longitude,'image_url': url})
        cameras.append({
            'id': cam_id,
            'image_url': url,
            'name': info,
            'latitude': latitude,
            'longitude': longitude
        })

    for camara in root.findall('camara'):
        cam_id = camara.find('id').text
        src = camara.find('src').text
        lugar = camara.find('lugar').text
        coordenadas = camara.find('coordenadas').text.split(',')
        latitude = coordenadas[0]
        longitude = coordenadas[1]

        camera ,created = Camera.objects.update_or_create(id=cam_id, defaults={'name': lugar, 'latitude': latitude, 'longitude': longitude,'image_url': src})
        cameras.append({
            'id': cam_id,
            'image_url': src,
            'name': lugar,
            'latitude': latitude,
            'longitude': longitude
        })

    print(cameras)

    return cameras
@csrf_exempt
def cameras(request):
    if request.method == "POST":
        print("Entro a POST")
        file_name = request.POST["file_name"]
        print(file_name)
        file_path = os.path.join(settings.BASE_DIR, 'teveo_app/static', file_name)
        camaras = read_xml(file_path)

    camaras = Camera.objects.all().order_by('-num_comments')
    camera_images = [camera.image_url for camera in camaras if camera.image_url]
    foto_random = random.choice(camera_images) if camera_images else None


    for camara in camaras:
        camara = Camera.objects.get(id=camara.id)
        comentarios = Comment.objects.filter(id_camera = camara)
        camara.num_comments = comentarios.count()
        camara.save()



    context = {
        "foto_random": foto_random,
        "source": DB_SOURCE,
        "cameras": camaras,
        "camera_count": Camera.objects.count(),
        "comment_count": Comment.objects.count()
    }
    return render(request, "camaras.html", context)

def download_image(url_imagen):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    response = requests.get(url_imagen, headers=headers)
    response.raise_for_status()

    imagen_base64 = base64.b64encode(response.content).decode('utf-8')
    devuelvo = f'data:image/jpeg;base64,{imagen_base64}'
    return devuelvo

def comentario(request):
    if request.method == "POST":
        camera_id = request.POST.get('camera_id')
        comment_text = request.POST.get('comment_text')
        author= request.POST.get('username')

        if not author:
            author = "Anónimo"

        camera = Camera.objects.get(id=camera_id)
        img_url = camera.image_url
        image_bytes = download_image(img_url)

        comment = Comment(id_camera=camera, text=comment_text, date=timezone.now(), image=image_bytes, author=author)
        comment.save()


        camera.num_comments += 1
        camera.save()

        return redirect('camera_details', camera_id=camera_id)



    camera_id = request.GET['camera_id']
    camera = Camera.objects.get(id=camera_id)
    comments = Comment.objects.filter(id_camera=camera)
    username = request.session.get('username', 'Anónimo')
    context = {
        'camera': camera,
        'comments': comments,
        'username': username,
        "camera_count": Camera.objects.count(),
        "comment_count": Comment.objects.count()
    }
    return render(request, "comentario.html", context)

def camera_details(request, camera_id):
    camera = Camera.objects.get(id = camera_id)
    comments = Comment.objects.filter(id_camera=camera).order_by('-date')

    context = {
        'camera': camera,
        'comments': comments,
        "camera_count": Camera.objects.count(),
        "comment_count": Comment.objects.count(),

    }
    return render(request, 'camera_especifica.html', context)

def configuracion(request):
    if request.method == "POST":
        username = request.POST.get('username')
        font_size = request.POST.get('font_size')
        font_type = request.POST.get('font_type')

        request.session['username'] = username
        request.session['font_size'] = font_size
        request.session['font_type'] = font_type

        return redirect('configuracion')

    context = {
        "camera_count": Camera.objects.count(),
        "comment_count": Comment.objects.count(),
    }
    return render(request, 'set_username.html',context)

def ayuda(request):
    context = {
        "camera_count": Camera.objects.count(),
        "comment_count": Comment.objects.count(),
    }
    return render(request, 'ayuda.html',context)

def camera_dynamic(request, camera_id):
    camera = Camera.objects.get(id=camera_id)
    context = {
        'camera': camera,
        "camera_count" : Camera.objects.count(),
        "comment_count": Comment.objects.count(),
        'comments_for_this_camera': Comment.objects.filter(id_camera=camera).order_by('-date'),
    }
    return render(request, 'camera_dynamic.html', context)

def descargar_y_devolver_imagen(request,camera_id):
    camera = get_object_or_404(Camera,id = camera_id)
    url_imagen = camera.image_url
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    response = requests.get(url_imagen, headers=headers)
    response.raise_for_status()

    imagen_base64 = base64.b64encode(response.content).decode('utf-8')
    devuelvo = f'<img src="data:image/jpeg;base64,{imagen_base64}" alt="Imagen de la cámara" class="camara-img">'

    return HttpResponse(devuelvo, content_type="text/html")

def camera_info_json(request, camera_id):
    camera = get_object_or_404(Camera, id=camera_id)
    comments_count = Comment.objects.filter(id_camera=camera).count()
    camera_data = {
        'id': camera.id,
        'name': camera.name,
        'latitude': camera.latitude,
        'longitude': camera.longitude,
        'image_url': camera.image_url,
        'comments_count': comments_count
    }
    return JsonResponse(camera_data)

def cerrar_sesion(request):
    logout(request)
    return redirect('index')

def votar_camera(request, camera_id):
    camera = get_object_or_404(Camera, id=camera_id)
    camera.votes += 1
    camera.save()
    return redirect('camera_details', camera_id=camera_id)

def generar_link(request):
    if request.session.session_key != None:
        dominio = request.get_host()
        cookie = request.session.session_key
        enlace = f"http://{dominio}/teveo/cambio/{cookie}"
        auth_link = {
            'auth_link': enlace,
        }
        return JsonResponse(auth_link)
    else:
        return redirect("configuracion")
def cambio(request,cookie):
    response = redirect('/teveo')  # Función para redirigir a la págin principal
    # Establezco la cookie
    # set_cookie es el método para establecer la cookie en la respuesta de sesión
    response.set_cookie(
        key='sessionid',  # nombre de la cookie de sesión
        value=cookie,  # valor de la cookie (especificado como parámetro en el enlace)
    )
    return response

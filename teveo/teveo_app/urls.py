from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path("",views.index,name = 'index'),
    path("camaras",views.cameras, name = 'camaras'),
    path("comentarios",views.comentario, name = 'comentario'),
    path("configuracion/", views.configuracion, name='configuracion'),
    path("camaras/<str:camera_id>-dyn/", views.camera_dynamic, name='camera_dynamic'),
    path("camaras/<str:camera_id>/", views.camera_details, name='camera_details'),
    path("ayuda/", views.ayuda, name='ayuda'),
    path('cerrar_sesion/', views.cerrar_sesion, name='cerrar_sesion'),
    path("descargar/<str:camera_id>/",views.descargar_y_devolver_imagen, name = 'descargar'),
    path("camaras/<str:camera_id>/info/", views.camera_info_json, name='camera_info_json'),
    path('camera/<str:camera_id>/vote/', views.votar_camera, name='votar_camera'),
    path('link/', views.generar_link, name='session'),
    path('cambio/<str:cookie>', views.cambio, name='cambio'),
]
